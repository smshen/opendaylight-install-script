#!/bin/bash

echo 'nameserver 8.8.8.8' >> /etc/resolv.conf
sudo apt-get update
sudo apt-get install -y curl wget unzip git
sudo apt-get install -y openjdk-7-jdk

# install mininet
git clone git://github.com/mininet/mininet
cd mininet
git tag  # list available versions
git checkout -b 2.2.1 2.2.1  # or whatever version you wish to install
cd ..
mininet/util/install.sh -a

# install mvn
sudo mkdir -p /usr/local/apache-maven
wget http://ftp.wayne.edu/apache/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
sudo mkdir /usr/local/apache-maven
sudo mv apache-maven-3.3.3-bin.tar.gz /usr/local/apache-maven
sudo tar -xzvf /usr/local/apache-maven/apache-maven-3.3.3-bin.tar.gz -C /usr/local/apache-maven/
sudo update-alternatives --install /usr/bin/mvn mvn /usr/local/apache-maven/apache-maven-3.3.3/bin/mvn 1
sudo update-alternatives --config mvn

# add config to bashrc
echo 'export M2_HOME="/usr/local/apache-maven/apache-maven-3.3.3"' >> ~/.bashrc
echo 'export MAVEN_OPTS="-Xms256m -Xmx512m" # Very important to put the "m" on the end' >> ~/.bashrc
echo 'export JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64" # This matches sudo update-alternatives --config java' >> ~/.bashrc

# export environment variable
export M2_HOME="/usr/local/apache-maven/apache-maven-3.3.3"
export MAVEN_OPTS="-Xms256m -Xmx512m"
export JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64"

# reload bashrc
source ~/.bashrc

mkdir ~/ODL
cd ~/ODL

# download tar.gz version for linux.
curl -O https://nexus.opendaylight.org/content/groups/public/org/opendaylight/integration/distribution-karaf/0.2.4-Helium-SR4/distribution-karaf-0.2.4-Helium-SR4.tar.gz

tar -xzvf distribution-karaf-0.2.4-Helium-SR4.tar.gz

cd ~/ODL/distribution-karaf-0.2.4-Helium-SR4/bin

# install features.
echo "feature:install odl-mdsal-clustering odl-restconf odl-l2switch-switch odl-openflowplugin-all odl-dlux-all odl-mdsal-all odl-adsal-northbound" | ./karaf

# restart karaf
./karaf


