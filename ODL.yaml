heat_template_version: 2014-10-16

parameters:
  my_keypair:
    type: string
    constraints:
      - custom_constraint: nova.keypair
  my_flavor:
    type: string
    constraints:
      - allowed_values:
        - 1core2GBmemory20GBdisk
        - 2cores4GBmemory40GBdisk
      - custom_constraint: nova.flavor
  my_volume_size:
    type: number
    constraints:
      - range:
          min: 10

resources:
  my_security_group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: ODL_security_group
      rules:
        - protocol: icmp
        - protocol: tcp
          port_range_max: 22
          port_range_min: 22
        - protocol: tcp
          port_range_max: 80
          port_range_min: 80
        - protocol: tcp
          port_range_max: 443
          port_range_min: 443
        - protocol: tcp
          port_range_max: 8080
          port_range_min: 8080
        - protocol: tcp
          port_range_max: 8181
          port_range_min: 8181
        - protocol: tcp
          port_range_max: 6633
          port_range_min: 6633
        - protocol: tcp
          port_range_max: 1099
          port_range_min: 1099
  my_private_net:
    type: OS::Neutron::Net
    properties:
      name: ODL_net
  my_private_subnet:
    type: OS::Neutron::Subnet
    properties:
      name: ODL_subnet
      network_id: { get_resource: my_private_net}
      cidr: 192.168.1.0/24
      gateway_ip: 192.168.1.254
  my_router:
    type: OS::Neutron::Router
    properties:
      name: ODL_router
      external_gateway_info:
        network: net04_ext
  my_router_interface:
    type: OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: my_router }
      subnet_id: { get_resource: my_private_subnet }
  my_volume:
    type: OS::Cinder::Volume
    properties:
      name: ODL_volume
      size: { get_param: my_volume_size }
  volume_attachment:
    type: OS::Cinder::VolumeAttachment
    properties:
      volume_id: { get_resource: my_volume }
      instance_uuid: { get_resource: my_instance }
  my_instance:
    type: OS::Nova::Server
    properties:
      name: ODL_instance
      flavor: { get_param: my_flavor}
      networks:
        - network: { get_resource: my_private_net }
      key_name: { get_param: my_keypair }
      image: Ubuntu14.04
      security_groups:
        - { get_resource: my_security_group }
      user_data_format: RAW
      user_data: |
        #!/bin/bash
        echo 'nameserver 8.8.8.8' >> /etc/resolv.conf
        sudo apt-get update
        sudo apt-get install -y curl wget unzip git
        sudo apt-get install -y openjdk-7-jdk

        # install mininet
        git clone git://github.com/mininet/mininet
        cd mininet
        git checkout -b 2.2.1 # or whatever version you wish to install
        cd ..
        mininet/util/install.sh -a

        # install mvn
        sudo mkdir -p /usr/local/apache-maven
        wget http://ftp.wayne.edu/apache/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
        sudo mkdir /usr/local/apache-maven
        sudo mv apache-maven-3.3.3-bin.tar.gz /usr/local/apache-maven
        sudo tar -xzvf /usr/local/apache-maven/apache-maven-3.3.3-bin.tar.gz -C /usr/local/apache-maven/
        sudo update-alternatives --install /usr/bin/mvn mvn /usr/local/apache-maven/apache-maven-3.3.3/bin/mvn 1
        sudo update-alternatives --config mvn

        # add config to bashrc
        echo 'export M2_HOME="/usr/local/apache-maven/apache-maven-3.3.3"' >> ~/.bashrc
        echo 'export MAVEN_OPTS="-Xms256m -Xmx512m" # Very important to put the "m" on the end' >> ~/.bashrc
        echo 'export JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64" # This matches sudo update-alternatives --config java' >> ~/.bashrc

        # export environment variable
        export M2_HOME="/usr/local/apache-maven/apache-maven-3.3.3"
        export MAVEN_OPTS="-Xms256m -Xmx512m"
        export JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64"

        # reload bashrc
        source ~/.bashrc

        mkdir ~/ODL
        cd ~/ODL

        # download tar.gz version for linux.
        curl -O https://nexus.opendaylight.org/content/groups/public/org/opendaylight/integration/distribution-karaf/0.2.4-Helium-SR4/distribution-karaf-0.2.4-Helium-SR4.tar.gz

        tar -xzvf distribution-karaf-0.2.4-Helium-SR4.tar.gz

        cd ~/ODL/distribution-karaf-0.2.4-Helium-SR4/bin

        # install features.
        echo "feature:install odl-mdsal-clustering odl-restconf odl-l2switch-switch odl-openflowplugin-all odl-dlux-all odl-mdsal-all odl-adsal-northbound" | ./karaf

        # restart karaf
        ./karaf
  ip_ass:
    type: OS::Nova::FloatingIPAssociation
    properties:
      floating_ip: { get_resource: my_floating_ip}
      server_id: { get_resource: my_instance }
  my_floating_ip:
    type: OS::Nova::FloatingIP
    properties:
      pool: net04_ext